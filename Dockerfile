FROM python:3.7

ADD . /app
WORKDIR /app
COPY . ./
#set directoty where CMD will execute
RUN pip install --no-cache-dir -r requirements.txt

# copy current dir's content to container's WORKDIR root i.e. all the contents of the web app
COPY . .