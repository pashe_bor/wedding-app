import * as React from 'react';

import Envelope from "./Components/Envelope";
import Letter from "./Components/Letter";

import './styles.scss';

const dataStub = {
    id: 1,
    name: "Александра & Софья",
    party: false,
    officialPart: false
};

const userData = window.__INITIAL_STATE__ ||  dataStub;

function App() {
  return (
    <div className="wedding-app">
        <div className="wedding-app__blur-bg" />
        <main className="wedding-body">
            <Envelope letter={<Letter userData={userData} />} />
        </main>
    </div>
  );
}

export default App;
