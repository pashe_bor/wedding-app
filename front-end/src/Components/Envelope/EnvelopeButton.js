import * as React from 'react';
import { ReactSVG } from "react-svg";

import Rings from '../../assets/images/rings.svg';

const EnvelopeButton = ({ onClick }) => {
    const [isClicked, setClicked] = React.useState(false);

    return (
        <button
            type="button" onClick={() => setClicked(true)}
            className={`envelope-button ${isClicked ? 'envelope-button--clicked' : ''}`}
            onTransitionEnd={onClick}
        >
            <ReactSVG className="envelope-button__icon" src={Rings}/>
        </button>
    )
};

export default EnvelopeButton;