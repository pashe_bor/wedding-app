import * as React from 'react';

import EnvelopeButton from "./EnvelopeButton";

import './styles.scss';

const Envelope = ({ letter }) => {
    const [isOpen, setOpen] = React.useState(false);

    const onOpenHandler = () => setOpen(true);

    return (
        <div className="envelope">
            <div className={`envelope__flap ${ isOpen ? 'envelope__flap-is-open' : '' }`}  />
            {
                isOpen
                    ? letter
                    : <EnvelopeButton onClick={onOpenHandler} />
            }
        </div>
    )
};

export default Envelope;