import * as React from 'react';

const renderNames = (name) => {
    if (name.includes('&')) {
        const nameSplitted = name.split(' &amp; ');
        return (
            <h2 className="letter__title">
                {nameSplitted[0]}
                <br />
                &
                <br />
                {nameSplitted[1]}
            </h2>
        )
    }

    return <h2 className="letter__title">{name}</h2>
};

const BackSide = ({ name }) => (
    <div className="letter-back-side">
        {renderNames(name)}
        <p className="letter__content">Будем рады видеть вас на первом празднике нашей семьи</p>
        <time className="letter__time">
            11.07.2020
            <hr className="letter__line" />
            14:00
        </time>
        <p className="letter__subtitle">ресторан "Маруся"</p>
        <p className="letter__content">
            Первомайский пр-т, д. 62А
            <br />
            г. Рязань
        </p>
    </div>
);

export default BackSide;