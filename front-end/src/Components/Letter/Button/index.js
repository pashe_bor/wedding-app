import * as React from 'react';

import './styles.scss';

const LetterButton = ({ text, onClick, isChecked }) => (
    <button
        className={`letter-button ${isChecked ? 'letter-button--checked' : '' }`}
        type="button"
        onClick={onClick}
    >
        {
            isChecked
                ? <span>&#x2713;</span>
                : null
        }
        {text}
    </button>
);

export default LetterButton;