import * as React from 'react';

const FrontSide = () => (
    <div className="letter-front-side">
        <h2 className="letter__title">
            Павел
            <br/>
            &
            <br/>
            Елена
        </h2>
        <p className="letter__content">Приглашают Вас на торжественную церемонию своего бракосочетания</p>
        <time className="letter__time">
            11.07.2020
            <hr className="letter__line" />
            10:30
        </time>
        <p className="letter__content">Адрес проведения:</p>
        <address className="letter__content">г. Рязань, Московское шоссе, д. 49, Лавандовый зал</address>
    </div>
);

export default FrontSide;