import * as React from 'react';

import transport from "../../utils/transport";

import LetterButton from './Button';
import FrontSide from './FrontSide';
import BackSide from './BackSide';

import './styles.scss';

const Letter = ({ userData }) => {
        const [isDeployed, setDeployed] = React.useState(false),
            [isFlipped, setFlipped] = React.useState(false),
            [party, setParty] = React.useState(userData.party),
            [officialPart, setOfficialPart] = React.useState(userData.officialPart);

    const sendAgreement = async () => {
        try {
            const data = {
                id: userData.id
            };

            if (isFlipped) {
                data.party = !party
            } else {
                data.officialPart = !officialPart;
            }

            const response = await transport.patch('/user-update/', data);

            if (response.data) {
                Object.keys(response.data).includes('party')
                    ? setParty(response.data.party)
                    : setOfficialPart(response.data.officialPart)
            }

        } catch (e) {
            console.log(e);
        }
    };

        return (
            <div
                className={`letter ${isDeployed ? 'letter--full' : ''} ${isFlipped ? 'letter--flipped' : ''}`}
                onAnimationEnd={() => setDeployed(true)}
            >
                <div className="letter__container">
                    {
                        isFlipped
                            ? <BackSide name={userData.name} />
                            : <FrontSide />
                    }
                    {
                        isDeployed && (
                            <div className="letter__btn-group">
                                <LetterButton text="Перевернуть" onClick={() => setFlipped(!isFlipped)}/>
                                <LetterButton
                                    text="Пойду"
                                    isChecked={
                                        isFlipped
                                            ? party
                                            : officialPart
                                    }
                                    onClick={sendAgreement}
                                />
                            </div>
                        )
                    }
                </div>
            </div>
        )
};

export default Letter;