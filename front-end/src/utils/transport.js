import axios from 'axios';

const transport = axios.create({
    withCredentials: true,
    xsrfHeaderName: 'X-CSRFToken',
    xsrfCookieName: 'csrftoken',
    'Content-Type': 'application/json'
});

export default transport;