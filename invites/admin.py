from django.contrib import admin

from .models import InvitedPersons


class AdminSite(admin.ModelAdmin):
    list_display = ('id', 'invitor_name', 'official_part', 'party')
    list_filter = ('invitor_name',)
    search_fields = ('invitor_name',)


admin.site.register(InvitedPersons, AdminSite)
