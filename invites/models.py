from django.db import models


class InvitedPersons(models.Model):
    invitor_name = models.CharField(max_length=200, verbose_name='Приглашенные гости')
    official_part = models.BooleanField(default=False, verbose_name="Торжественная часть")
    party = models.BooleanField(default=False, verbose_name="Вечеринка")

    class Meta:
        verbose_name = 'Гость'
        verbose_name_plural = 'Гости'
