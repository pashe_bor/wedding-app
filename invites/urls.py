from django.urls import path
from . import views

urlpatterns = [
    path('user-update/', views.update_user_data, name='update'),
    path('<int:id>/', views.get_person_by_id, name='index')
]
