import json

from django.db.models import Model
from django.shortcuts import render
from django.http import JsonResponse

from .models import InvitedPersons


def update_user_data(request):
    data = json.loads(request.body)

    try:
        user_obj = InvitedPersons.objects.get(pk=data['id'])

        if 'party' in data:
            user_obj.party = data['party']
            user_obj.save()
        elif 'officialPart' in data:
            user_obj.official_part = data['officialPart']
            user_obj.save()

        return JsonResponse(data)
    except Model.DoesNotExist:
        return JsonResponse({'message': 'Этого человека нет в списке приглашенных!'})


def get_person_by_id(request, id):
    person = InvitedPersons.objects.get(pk=id)

    context = {
        'id': id,
        'person_name': person.invitor_name,
        'party': person.party,
        'official_part': person.official_part
    }

    return render(request, 'main_page/index.html', context)
